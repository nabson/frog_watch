var request = require('sync-request');
var read = require('read-file');
var LineByLineReader = require('line-by-line'),
lr = new LineByLineReader('SpringPeeper_original.csv');

var key = 'c4a335d3efa1ca5e'; // conta free
//var key = 'c137e42a99ef99f0'; // conta comprada plan Drizzle comprada o dia 16/11/2015

console.log('date,lat,lng,alt,meantempm,meandewptm,meanpressurem,meanwindspdm,meanwdird,meanvism,humidity,maxtempm,mintempm,maxhumidity,minhumidity,maxdewptm,mindewptm,maxpressurem,minpressurem,maxwspdm,minwspdm,maxvism,minvism,idSpecies'); /** Imprime cabeçalho com o nome das variaveis */

lr.on('line', function (line) {
	lr.pause();
	var arr = line.split(",");
	VARIABLE_CORRELATED(arr[0], arr[1], arr[2], arr[3]);
	sleep(10000); // espera 10seg antes da proxima consulta
	lr.resume();
});

function sleep(ms) {
    var unixtime_ms = new Date().getTime();
    while(new Date().getTime() < unixtime_ms + ms) {}
}

function altitude(latitude,longitude){
	var res = request('GET', 'https://maps.googleapis.com/maps/api/elevation/json?locations='+latitude+','+longitude+'&key=AIzaSyA5R_LsyaoUtq8hUgR9LvU_SHPyBFhdbuQ',JSON);
	var info = JSON.parse(res.getBody());
	return info.results[0].elevation;
}

function VARIABLE_CORRELATED(date, lat, lng, ID){
	var res = request('GET', 'http://api.wunderground.com/api/' + key + '/history_' + date + '/q/' + lat + ',' + lng + '.json',JSON);
	var info = JSON.parse(res.getBody());	
	if (res.statusCode == 200){
		if (info.history !== undefined){
			if (info.history.dailysummary[0] !== undefined) {
				var meantempm = info.history.dailysummary[0].meantempm; //mean temperature in celcius (a metric unit)
				var meandewptm = info.history.dailysummary[0].meandewptm;
				var meanpressurem = info.history.dailysummary[0].meanpressurem;
				var meanwindspdm = info.history.dailysummary[0].meanwindspdm;
				var meanwdird = info.history.dailysummary[0].meanwdird;
				var meanvism = info.history.dailysummary[0].meanvism;
				var humidity = info.history.dailysummary[0].humidity;
				var maxtempm = info.history.dailysummary[0].maxtempm;
				var mintempm = info.history.dailysummary[0].mintempm;
				var maxhumidity = info.history.dailysummary[0].maxhumidity;
				var minhumidity = info.history.dailysummary[0].minhumidity;
				var maxdewptm = info.history.dailysummary[0].maxdewptm;
				var mindewptm = info.history.dailysummary[0].mindewptm;
				var maxpressurem = info.history.dailysummary[0].maxpressurem;
				var minpressurem = info.history.dailysummary[0].minpressurem;					
				var maxwspdm = info.history.dailysummary[0].maxwspdm;					
				var minwspdm = info.history.dailysummary[0].minwspdm;					
				var maxvism = info.history.dailysummary[0].maxvism;
				var minvism = info.history.dailysummary[0].minvism;
				
				var alt = altitude(lat,lng);	
				
				console.log(date + ',' + lat + ',' + lng + ',' + alt + ',' + meantempm + ',' + meandewptm + ',' + meanpressurem + ',' + meanwindspdm + ',' + meanwdird + ',' + meanvism + ',' + humidity + ',' + maxtempm + ',' + mintempm + ',' + maxhumidity + ',' + minhumidity + ',' + maxdewptm + ',' + mindewptm + ',' + maxpressurem + ',' + minpressurem + ',' + maxwspdm + ',' + minwspdm + ',' + maxvism + ',' + minvism + ',' + ID);	

			} else { }
		} else { }
	} else { }
}


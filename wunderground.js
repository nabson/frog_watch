var request = require('sync-request');
var read = require('read-file');
var LineByLineReader = require('line-by-line'),
lr = new LineByLineReader('BaseParcial_1.csv');
// lr = new LineByLineReader('BaseParcial_2.csv');
// lr = new LineByLineReader('BaseParcial_3.csv');
// lr = new LineByLineReader('BaseParcial_4.csv');
// lr = new LineByLineReader('BaseParcial_5.csv');
// var key = 'c4a335d3efa1ca5e'; // conta free
var key = 'c137e42a99ef99f0'; // conta comprada plan Drizzle comprada o dia 16/11/2015

console.log('date,lat,lng,h,meantempm,meandewptm,meanpressurem,meanwindspdm,meanwdird,meanvism,humidity,maxtempm,mintempm,maxhumidity,minhumidity,maxdewptm,mindewptm,maxpressurem,minpressurem,maxwspdm,minwspdm,maxvism,minvism,idSpecies'); /** Imprime cabeçalho com o nome das variaveis */

lr.on('line', function (line) {
	lr.pause();
	var arr = line.split(",");
	var ambiental = VARIABLE_CORRELATED(arr[0], arr[1], arr[2]);
	if (ambiental !== null) {
		console.log(arr[0] + ',' + arr[1] + ',' + arr[2] + ',' + ambiental + ',' + arr[3]);	
	}
	sleep(2000); // espera 6seg antes da proxima consulta
	lr.resume();
});

function sleep(ms) {
    var unixtime_ms = new Date().getTime();
    while(new Date().getTime() < unixtime_ms + ms) {}
}

function VARIABLE_CORRELATED(date, lat, lng){
	var res = request('GET', 'http://api.wunderground.com/api/' + key + '/history_' + date + '/q/' + lat + ',' + lng + '.json',JSON);
	var info = JSON.parse(res.getBody());	
	if (res.statusCode == 200){
		if (info.history !== undefined){
			if (info.history.dailysummary[0] !== undefined) {
				var meantempm = info.history.dailysummary[0].meantempm; //mean temperature in celcius (a metric unit)
				var meandewptm = info.history.dailysummary[0].meandewptm;
				var meanpressurem = info.history.dailysummary[0].meanpressurem;
				var meanwindspdm = info.history.dailysummary[0].meanwindspdm;
				var meanwdird = info.history.dailysummary[0].meanwdird;
				var meanvism = info.history.dailysummary[0].meanvism;
				var humidity = info.history.dailysummary[0].humidity;
				var maxtempm = info.history.dailysummary[0].maxtempm;
				var mintempm = info.history.dailysummary[0].mintempm;
				var maxhumidity = info.history.dailysummary[0].maxhumidity;
				var minhumidity = info.history.dailysummary[0].minhumidity;
				var maxdewptm = info.history.dailysummary[0].maxdewptm;
				var mindewptm = info.history.dailysummary[0].mindewptm;
				var maxpressurem = info.history.dailysummary[0].maxpressurem;
				var minpressurem = info.history.dailysummary[0].minpressurem;					
				var maxwspdm = info.history.dailysummary[0].maxwspdm;					
				var minwspdm = info.history.dailysummary[0].minwspdm;					
				var maxvism = info.history.dailysummary[0].maxvism;
				var minvism = info.history.dailysummary[0].minvism;
				
				return [meantempm, meandewptm, meanpressurem, meanwindspdm, meanwdird, meanvism, humidity, maxtempm, mintempm, 
				maxhumidity, minhumidity, maxdewptm, mindewptm, maxpressurem, minpressurem, maxwspdm, minwspdm, maxvism, minvism];
	
			} else { return null;}
		} else { return null;}
	} else { return null;}
}

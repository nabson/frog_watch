Download dos dados de https://www.naturewatch.ca/frogwatch/
no dia 15 de nov 2015

Data collected by NatureWatch.ca are for research and education purposes (not for profit), and are generally:

- Open access
- Free to anyone
Ownership of the data lies with NatureWatch.ca

Citation request
Published data shall be cited as below. It is important to include the download date in citations since data may be subject to later updates.

Example:
NatureWatch Canada, http://www.naturewatch.ca, PlantWatch data from 7 June 2008 to 21 October 2012, University of Ottawa, Canada. Downloaded on 25 April 2014.


/usr/bin/nodejs wunderground.js >> FrogWatch_correlated_varivel_1.csv
/usr/bin/nodejs wunderground.js >> FrogWatch_correlated_varivel_2.csv
/usr/bin/nodejs wunderground.js >> FrogWatch_correlated_varivel_3.csv
/usr/bin/nodejs wunderground.js >> FrogWatch_correlated_varivel_4.csv
/usr/bin/nodejs wunderground.js >> FrogWatch_correlated_varivel_5.csv

Base final juntando todas as coletas >> FrogWatch_correlated_varivel.csv
